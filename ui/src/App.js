import React from 'react';
import Navbar from './Components/Navbar/NavBar'
import './App.css';
import Routing from './Router/Router';

function App() {
  return (
    <div className="App">
       <Navbar></Navbar>
      <Routing></Routing>
    </div>
  );
}

export default App;
