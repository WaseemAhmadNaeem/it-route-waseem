import React from 'react';
import {Route, BrowserRouter} from 'react-router-dom';
import List from '../Components/List/List';
import Create from '../Components/Create/Create';
import { Container } from '@material-ui/core';
import Modify from '../Components/Modify/Modify';

export default function Routing (){
    return(
                <Container>
                        <Route exact path="/" component={List}/>
                        <Route path="/create" component={Create}/>
                        <Route path="/modify/:id" component={Modify}/>
                </Container>
    )
}