import React from 'react';
import axios from 'axios';
import { Card, Typography, TextField, Button } from '@material-ui/core';
import { RouteProps } from 'react-router';
import { Redirect } from 'react-router-dom';

interface Props {
    id? : string
};

class Modify extends React.Component<Props & RouteProps>
{
    state = {
        id : "",
        name : "",
        number : "",
        email : "",
        redirect : false
    }

    renderRedirect(){
        if(this.state.redirect){
            return <Redirect to="/" />
        }
    }
    componentDidMount(){
        var id = this.props as any;
        id = id['match'].params.id;
        axios.get("https://localhost:5001/Contact?id="+id).then(res => {
            const data = res.data[0];
            console.log(data)
            this.setState({id : data.id})
            this.setState({name : data.name})
            this.setState({number : data.number})
            this.setState({email : data.email});

        }).catch(err => {
            alert("Error while downloading data, Please refresh this page")
        })
    }
    render(){
        
        return(
            <div className="">
                {this.renderRedirect()}
                <Card >
                <form noValidate autoComplete="off" className="center">
                    <Typography variant="h5">
                            Modify Record
                    </Typography>
                    <div className="spacing">
                        <TextField value={this.state.name} onChange={(val) => this.setState({name : val.target.value})} label="Name" variant="outlined"></TextField>
                    </div>
                    <div className="spacing">
                        <TextField value={this.state.email} onChange={val => this.setState({email : val.target.value})} label="Email" variant="outlined"></TextField>
                    </div>
                    <div className="spacing">
                        <TextField value={this.state.number} onChange={val => this.setState({number : val.target.value})} label="Number" variant="outlined"></TextField>
                    </div>
                    <div className="spacing">
                        <Button variant="outlined" onClick={()=>{
                            if(this.state.number.trim() != "" && this.state.email.trim() != "" && this.state.name.trim() != "")
                            {
                                axios.put("https://localhost:5001/Contact",{
                                    id : this.state.id,
                                    name : this.state.name,
                                    email : this.state.email,
                                    number : this.state.number
                                }).then(res => {
                                    this.setState({redirect : true});
                                })
                                .catch(err => alert("Could not save number, Please try again"));
                            }
                            else{
                                alert("Please fill all values")
                            }
                        }} color="primary"> Modify </Button>
                    </div>
                </form>
            </Card>
            </div>
        )
    }
};

export default Modify;