import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import { Toolbar, InputBase, IconButton,  Typography } from '@material-ui/core';
import CreateIcon from '@material-ui/icons/Create';
import {Link } from 'react-router-dom';


class Navbar extends React.Component {
    
    render() {
       
        return (
            <AppBar position="static"
                
            >
                <Toolbar>
                    <Typography variant="h6" noWrap>
                        <Link to="/">
                            Contacts
                        </Link>
                    </Typography>

                    <IconButton edge="start"  >
                        <Link to="/create">
                            <CreateIcon color="action"/>
                        </Link>
                    </IconButton> 
                </Toolbar>
            </AppBar>
        )
    }
}

export default Navbar;