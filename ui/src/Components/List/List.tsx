import React from 'react';
import axios from 'axios';
import {Link, RouteProps } from 'react-router-dom'
import { Card, Container, Paper, TableContainer, Table, TableRow, TableHead, TableCell, TableBody, Button } from '@material-ui/core';



class List extends React.Component{
    state = {
        contacts : []
    }
    componentDidMount() {
        this.LoadData();
    }

    LoadData(){
        axios.get('https://localhost:5001/Contact')
        .then(res => {
            const contacts = res.data;
            this.setState({contacts});
        })
    }

    tableData = () => {
        return this.state.contacts.map(el => (
            <TableRow>
                <TableCell>{el['name']}</TableCell>
                <TableCell>{el['email']}</TableCell>
                <TableCell>{el['number']}</TableCell>
                <TableCell>
                   <Link  to={"/modify/"+el["id"]} > Update</Link>
                </TableCell>
                <TableCell>
                    <Button onClick={()=> {
                        axios.delete("https://localhost:5001/Contact?id="+el['id']).then(() =>{
                            this.LoadData();
                        }).catch(er => {
                            alert("error while deletion of record")
                        })
                    }} variant="outlined" color="secondary"> Delete</Button>
                </TableCell>
            </TableRow>
        ))
    }

    render(){
        return (
            <Container>
                <Paper>
                    <TableContainer>
                        <Table>
                            <TableHead>
                                <TableRow>
                                    <TableCell>Name</TableCell>
                                    <TableCell>Email</TableCell>
                                    <TableCell>Phone #</TableCell>
                                    <TableCell>Update</TableCell>
                                    <TableCell>Delete</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {this.tableData()}
                            </TableBody>
                        </Table>
                    </TableContainer>
                </Paper>
            </Container>
        )
    }
}

export default List;