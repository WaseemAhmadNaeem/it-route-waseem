import React from 'react';
import { TextField, Card, Typography, Button } from '@material-ui/core';
import axios from 'axios';
import {Redirect} from 'react-router-dom';

class Create extends React.Component
{
    state = {
        name : "",
        email : "",
        number : "",
        redirect : false,
    }

    renderRedirect()
    {
        if(this.state.redirect){
            return <Redirect to="/" />
        }
    }
    render(){
        return(
            <Card >
                {this.renderRedirect()}
                <form noValidate autoComplete="off" className="center">
                    <Typography variant="h5">
                            Create number
                    </Typography>
                    <div className="spacing">
                        <TextField value={this.state.name} onChange={(val) => this.setState({name : val.target.value})} label="Name" variant="outlined"></TextField>
                    </div>
                    <div className="spacing">
                        <TextField value={this.state.email} onChange={val => this.setState({email : val.target.value})} label="Email" variant="outlined"></TextField>
                    </div>
                    <div className="spacing">
                        <TextField value={this.state.number} onChange={val => this.setState({number : val.target.value})} label="number Number" variant="outlined"></TextField>
                    </div>
                    <div className="spacing">
                        <Button variant="outlined" onClick={()=>{
                            if(this.state.number.trim() != "" && this.state.email.trim() != "" && this.state.name.trim() != "")
                            {
                                axios.post("https://localhost:5001/Contact",{
                                    name : this.state.name,
                                    email : this.state.email,
                                    number : this.state.number
                                }).then(res => {
                                    this.setState({
                                        name : "",
                                        number : "",
                                        email : ""
                                    });
                                    this.setState({redirect : true})
                                })
                                .catch(err => alert("Could not save number, Please try again"));
                            }
                            else{
                                alert("Please fill all values")
                            }
                        }} color="primary"> Submit </Button>
                    </div>
                </form>
            </Card>
        )
    }
}

export default Create;