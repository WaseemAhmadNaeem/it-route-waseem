namespace ContactManager.Enumerations
{
    public enum Status
    {
        AVAILABLE,
        DELETED
    }
}