using Microsoft.AspNetCore.Mvc;
using ContactManager.Data;
using System.Collections.Generic;
using ContactManager.Respoistories;
using System.Linq;

namespace api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ContactController : ControllerBase
    {
        ContactRepoistory repoistory;

        public ContactController()
        {
            repoistory = new ContactRepoistory();
        }

        [HttpGet]
        public IEnumerable<Contact> Get(string id){
            
            if(string.IsNullOrEmpty(id))
                return repoistory.GetAll();
            return repoistory.GetAll().Where(el => el.Id == id);
            
        }

        [HttpPost]
        public ActionResult<Contact> Create(Contact contact)
        {
            return repoistory.Create(contact);
        }

        [HttpDelete]
        public IActionResult Delete(string id)
        {
            return repoistory.Delete(id) ? StatusCode(200) : StatusCode(404);  
        }

        [HttpPut]
        public ActionResult<Contact> Update(Contact contact)
        {
            return repoistory.Update(contact);
        }


    }

}