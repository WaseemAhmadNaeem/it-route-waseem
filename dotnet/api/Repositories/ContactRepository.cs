using System;
using ContactManager.Data;
using ContactManager.Context;
using System.Linq;
using System.Collections.Generic;
using ContactManager.Enumerations;

namespace ContactManager.Respoistories{
    public class ContactRepoistory
    {
        ContactContext context = new ContactContext();

        public bool Delete(string id){
            bool result = true;
            Contact temp = context.Contacts.FirstOrDefault(el => el.Id == id);
            if(temp != null){
                temp.Status = Status.DELETED;
                try{
                    context.SaveChanges();
                }catch{
                    result = false;
                }
                return result;
            }
            return false;
        }

        public Contact Update(Contact contact){
            bool result = true;
            Contact prev = context.Contacts.FirstOrDefault(el => el.Id == contact.Id && el.Status == Status.AVAILABLE) ;
            contact.Created = prev.Created;
            contact.Modified = DateTime.Now;
            contact.Status = prev.Status;

            if(prev != null){
                prev.Modified = DateTime.Now;
                prev.Name = contact.Name;
                prev.Email = contact.Email;
                prev.Number = contact.Number;

                try{
                    context.SaveChanges();
                }catch{
                    result = false;
                }

                return result ? prev : null;
            }
            return null;
        }
        public Contact Get(string id){
            return context.Contacts.FirstOrDefault(el => el.Id == id && el.Status == Status.AVAILABLE);
        }

        public List<Contact> GetAll(){
            return context.Contacts.Where(el => el.Status == Status.AVAILABLE).ToList();
        }

        public Contact Create(Contact contact)
        {
            bool result = false;
            contact.Id = Guid.NewGuid().ToString().Replace("-","");
            contact.Created = DateTime.Now;
            contact.Modified = DateTime.Now;
            contact.Status = Status.AVAILABLE;
            context.Add(contact);

            try{
                context.SaveChanges();
                result = true;

            }catch{
                result = false;
            }

            return result ? contact : null;
        }
    }
}