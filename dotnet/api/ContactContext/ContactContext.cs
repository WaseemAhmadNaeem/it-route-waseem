using System;
using Microsoft.EntityFrameworkCore;
using ContactManager.Data;

namespace ContactManager.Context
{
    public class ContactContext : DbContext
    {
        public DbSet<Contact> Contacts { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder options) => options.UseSqlite("Data Source = Contacts.db");     
    }
}